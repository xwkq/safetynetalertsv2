package com.safetynetalerts.alert.repositories;

import com.safetynetalerts.alert.SafetynetalertsApplication;
import com.safetynetalerts.alert.interfaces.services.IPersonService;
import com.safetynetalerts.alert.models.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application.properties")
class IPersonRepositoryTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private IPersonService personService;



    @BeforeEach
    void setUp() {
    }

    @Test
    void create() {

    }

}