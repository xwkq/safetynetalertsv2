package com.safetynetalerts.alert.services;

import com.safetynetalerts.alert.SafetynetalertsApplication;
import com.safetynetalerts.alert.interfaces.repositories.IMedicalRecordRepository;
import com.safetynetalerts.alert.interfaces.services.IMedicalRecordService;
import com.safetynetalerts.alert.models.MedicalRecord;

import com.safetynetalerts.alert.models.MedicalRecordsJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service

public class MedicalRecordService implements IMedicalRecordService {
    @Autowired
    private static final Logger logger = LogManager.getLogger(SafetynetalertsApplication.class);
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final List<MedicalRecordsJson> medicalRecords;

    public MedicalRecordService(IMedicalRecordRepository medicalRecordRepository){
        this.medicalRecords = medicalRecordRepository.getMedicalRecords();
    }

    @Override
    public List<MedicalRecord> findAll() {
        List<MedicalRecord> medicalRecords = new ArrayList<>();
        for(MedicalRecordsJson md : this.medicalRecords){
            MedicalRecord medicalRecord = new MedicalRecord();
            medicalRecord.setLastName(md.getLastName());
            medicalRecord.setFirstName(md.getFirstName());
            medicalRecord.setMedications(md.getMedications());
            medicalRecord.setAllergies(md.getAllergies());
            medicalRecord.setBirthdate(md.getBirthdate());
            medicalRecords.add(medicalRecord);
        }
        return medicalRecords;
    }

    public List<MedicalRecord> create(MedicalRecord medicalRecord) {
        List<MedicalRecord> medicalRecords = this.findAll();
        medicalRecords.add(medicalRecord);
        return medicalRecords;
    }

    public List<MedicalRecord> remove(MedicalRecord medicalRecord, String firstName, String lastName) {
        List<MedicalRecord> medicalRecords = this.findAll();
        for(MedicalRecord mRecord : medicalRecords){
            if(mRecord.getFirstName().equals(firstName) && mRecord.getLastName().equals(lastName)){
                medicalRecords.remove(mRecord);
                return medicalRecords;
            }
        }
        return medicalRecords;
    }

    public List<MedicalRecord> update(MedicalRecord medicalRecord, String firstName, String lastName) {

        List<MedicalRecord> medicalRecords = this.findAll();
        for(MedicalRecord mRecord : medicalRecords){
            if(mRecord.getFirstName().equals(firstName) && mRecord.getLastName().equals(lastName)){
                mRecord.setBirthdate(medicalRecord.getBirthdate());
                mRecord.setMedications(medicalRecord.getMedications());
                mRecord.setAllergies(medicalRecord.getAllergies());
            }
        }
        return medicalRecords;
    }

}
