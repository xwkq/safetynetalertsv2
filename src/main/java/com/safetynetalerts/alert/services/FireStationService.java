package com.safetynetalerts.alert.services;

import com.safetynetalerts.alert.interfaces.repositories.IFireStationRepository;
import com.safetynetalerts.alert.interfaces.repositories.IMedicalRecordRepository;
import com.safetynetalerts.alert.interfaces.services.IFireStationService;
import com.safetynetalerts.alert.interfaces.services.IPersonService;
import com.safetynetalerts.alert.models.FireStation;
import com.safetynetalerts.alert.models.MedicalRecord;
import com.safetynetalerts.alert.models.MedicalRecordsJson;
import com.safetynetalerts.alert.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class FireStationService implements IFireStationService {
    @Autowired
    private final IFireStationRepository fireStationRepository;
    private final IPersonService personService;
    private final List<FireStation> fireStations;
    private final List<MedicalRecordsJson> medicalRecords;

    public FireStationService(IFireStationRepository fireStationRepository, IPersonService personService, IMedicalRecordRepository medicalRecordRepository){

        this.fireStationRepository = fireStationRepository;
        this.personService = personService;
        this.medicalRecords = medicalRecordRepository.getMedicalRecords();
        this.fireStations = this.fireStationRepository.getFireStations();
    }

    public List<FireStation> findAll() {
        return this.fireStations;
    }

    public List<FireStation> create(FireStation fireStationModel){
        this.fireStations.add(fireStationModel);
        return fireStations;
    }

    public List<FireStation> update(FireStation fireStationModel, String address){
        String add = address.replace("+", " ");
        for (FireStation fireStation : fireStations ) {
            if(fireStation.getAddress().equals(add)){
                fireStation.setStation(fireStationModel.getStation());
                return this.fireStations;
            }
        }
        return this.fireStations;
    }

    public List<FireStation> findByAddress(String address){
        return null;
    }

    public List<FireStation> remove(FireStation fireStationModel, String address, String station){
        String add = address.replace("+", " ");
        for (FireStation fireStation : this.fireStations){
            if(fireStation.getAddress().equals(add) && fireStation.getStation().equals(station)){
                this.fireStations.remove(fireStation);
                return this.fireStations;
            }
        }
        return this.fireStations;
    }

    public FireStation findByStation(String station){
        FireStation fireStation = new FireStation();
        for (FireStation f : this.fireStations){
            if(f.getStation().equals(station)){
                fireStation = f;
            }
        }
        return fireStation;
    }

    public List<Person> getPersonByStations(List<String> stations){
        List<Person> persons =  new ArrayList<>();
        for (String station : stations ) {
            for(FireStation s : this.fireStations){
                for (Person p : this.personService.persons()){
                    if(s.getStation().equals(station) && s.getAddress().equals(p.getAddress())){
                        persons.add(p);
                    }
                }
            }
        }
        return persons;
    }
    public List<Person> getPersonByStation(String station){
        List<Person> personList = new ArrayList<>();
        for (Person p : this.personService.persons()){
            Person person = p;
            for(MedicalRecordsJson md : this.medicalRecords){
                MedicalRecord medicalRecord = new MedicalRecord();
                if(Objects.equals(md.getFirstName(), p.getFirstName()) && Objects.equals(md.getLastName(), p.getLastName())){
                    medicalRecord.setFirstName(md.firstName);
                    medicalRecord.setLastName(md.lastName);
                    medicalRecord.setBirthdate(md.getBirthdate());
                    medicalRecord.setMedications(md.medications);
                    medicalRecord.setAllergies(md.allergies);
                    person.setMedicalRecord(medicalRecord);
                }
            }
            for(FireStation f : this.fireStations){
                FireStation fireStation = f;
                if(p.getAddress().equals(f.getAddress())){
                    person.setFireStation(fireStation);
                }
            }
            if(person.getFireStationModel().getStation().equals(station)){
                personList.add(person);
            }
        }
        return personList;
    }
}
