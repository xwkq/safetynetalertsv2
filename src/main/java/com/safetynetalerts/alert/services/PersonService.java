package com.safetynetalerts.alert.services;


import com.safetynetalerts.alert.interfaces.repositories.IFireStationRepository;
import com.safetynetalerts.alert.interfaces.repositories.IMedicalRecordRepository;
import com.safetynetalerts.alert.interfaces.repositories.IPersonRepository;
import com.safetynetalerts.alert.interfaces.services.IPersonService;
import com.safetynetalerts.alert.models.*;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PersonService implements IPersonService {
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final List<Person> persons;
    private final List<FireStation> fireStations;
    private final List<MedicalRecordsJson> medicalRecords;

    public PersonService(IPersonRepository personRepository, IMedicalRecordRepository medicalRecordRepository, IFireStationRepository fireStationRepository){
        this.persons = personRepository.getPersons();
        this.fireStations = fireStationRepository.getFireStations();
        this.medicalRecords = medicalRecordRepository.getMedicalRecords();

    }

    public List<Person> persons (){
        return this.persons;
    }
    public List<Person> create(Person personModel) {
        this.persons.add(personModel);
        return persons;
    }

    public List<Person> remove(Person personModel, String firstName, String lastName) {
        for(Person person : this.persons){
            if(person.getFirstName().equals(firstName) && person.getLastName().equals(lastName)){
                persons.remove(person);
                return this.persons;
            }
        }
        return this.persons;
    }

    public List<Person> update(Person personModel, String firstName, String lastName) {
        for(Person person : this.persons){
            if(person.getFirstName().equals(firstName) && person.getLastName().equals(lastName)){
                person.setAddress(personModel.getAddress());
                person.setCity(personModel.getCity());
                person.setEmail(personModel.getEmail());
                person.setPhone(personModel.getPhone());
                person.setZip(personModel.getZip());
            }
        }
        return this.persons;
    }

    private List<Person> getPersonSameLastname(String lastName){
        return null;
    }

    public List<Person> getPersonInfo(String firstName, String lastName){
        List<Person> personInfo = new ArrayList<>();
        for (Person p : this.persons){
            Person person = p;
            for(MedicalRecordsJson md : this.medicalRecords){
                MedicalRecord medicalRecord = new MedicalRecord();
                if(Objects.equals(md.getFirstName(), p.getFirstName()) && Objects.equals(md.getLastName(), p.getLastName())){
                    medicalRecord.setFirstName(md.firstName);
                    medicalRecord.setLastName(md.lastName);
                    medicalRecord.setBirthdate(md.getBirthdate());
                    medicalRecord.setMedications(md.medications);
                    medicalRecord.setAllergies(md.allergies);
                    person.setMedicalRecord(medicalRecord);
                }
            }
            for(FireStation f : this.fireStations){
                FireStation fireStation = f;
                if(p.getAddress().equals(f.getAddress())){
                    person.setFireStation(fireStation);
                }
            }
            if(person.getFirstName().equals(firstName) && person.getLastName().equals(lastName))
                personInfo.add(person);
        }
        return personInfo;
    }

    public List<Person> findChild(String address){
        List<Person> persons = new ArrayList<>();
        for (Person p : this.persons){
            if(p.getAddress().equals(address)){
                LocalDate birthDate = LocalDate.parse(p.getMedicalRecord().getBirthdate(), dateTimeFormatter);
                for(Person child : this.getPersonInfo(p.getFirstName(), p.getLastName())){

                }
            }
        }
        return null;
    }
    public List<Person> findPersonByFireStationAddress(String address){
        List<Person> personList = new ArrayList<>();
        for (Person p : this.persons()){
            Person person = p;
            for(MedicalRecordsJson md : this.medicalRecords){
                MedicalRecord medicalRecord = new MedicalRecord();
                if(Objects.equals(md.getFirstName(), p.getFirstName()) && Objects.equals(md.getLastName(), p.getLastName())){
                    medicalRecord.setFirstName(md.firstName);
                    medicalRecord.setLastName(md.lastName);
                    medicalRecord.setBirthdate(md.getBirthdate());
                    medicalRecord.setMedications(md.medications);
                    medicalRecord.setAllergies(md.allergies);
                    person.setMedicalRecord(medicalRecord);
                }
            }
            for(FireStation f : this.fireStations){
                FireStation fireStation = f;
                if(p.getAddress().equals(f.getAddress())){
                    person.setFireStation(fireStation);
                }
            }
            if(person.getFireStationModel().getAddress().equals(address)){
                personList.add(person);
            }
        }
        return personList;
    }
    public List<Contact> findPhoneByStation(String station){

        List<Contact> contacts = new ArrayList<>();
        for (FireStation fs : this.fireStations){
            if(fs.getStation().equals(station)){
                Contact contact = new Contact();
                contact.setPhoneNumber(this.getPersonByAddress(fs.getAddress()).getPhone());
                contacts.add(contact);
            }
        }
        return contacts;
    }

    private Person getPersonByAddress(String address){
        Person person = new Person();
        for (Person p : this.persons){
            if(p.getAddress().equals(address)){
                person = p;
            }
        }
        return person;
    }
    public List<Contact> findEmailByCity(String city){

        List<Contact> contacts = new ArrayList<>();
        for (Person p : this.persons){
            Contact contact = new Contact();
            if(p.getCity().equals(city)){
                contact.setEmail(p.getEmail());
                contacts.add(contact);
            }
        }
        return contacts;
    }
}
