package com.safetynetalerts.alert.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class ConvertLocalDateToDate {
    public ConvertLocalDateToDate(){}
    public Date convertToDateUsingInstant(LocalDate date) {
        return java.util.Date.from(date.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
