package com.safetynetalerts.alert.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

import java.util.Date;
import java.util.List;


public class MedicalRecord {

    private Long id;
    private String firstName;
    private String lastName;
    private String birthdate;
    private List<String> allergies;
    private List<String> medications;

    public MedicalRecord() {
    }

    public MedicalRecord(String birthDate, List<String> allergie, List<String> medication, String firstName, String lastName){
        this.birthdate = birthDate;
        this.allergies = allergie;
        this.medications = medication;
        this.firstName = firstName;
        this.lastName = lastName;

    }
    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public List<String> getMedications() {
        return medications;
    }
    public void setMedications(List<String> medications) {
        this.medications = medications;
    }
    public List<String> getAllergies() {
        return allergies;
    }
    public void setAllergies(List<String> allergies) {
        this.allergies = allergies;
    }
    public String getFirstName(){
        return this.firstName;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public String getLastName(){
        return this.lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
}
