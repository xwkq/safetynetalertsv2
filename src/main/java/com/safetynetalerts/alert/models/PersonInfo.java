package com.safetynetalerts.alert.models;

public class PersonInfo {
    private String name;
    private String Address;
    private MedicalRecord medicalRecord;
    private String email;
    public PersonInfo(){}
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public MedicalRecord getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(MedicalRecord medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String emails) {
        this.email = emails;
    }

}
