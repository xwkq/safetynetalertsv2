package com.safetynetalerts.alert.models;

public class Contact {
    private String email;
    private String phoneNumber;

    public Contact(){

    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public String getPhoneNumber(){
        return this.phoneNumber;
    }
    public void setPhoneNumber(String number){
        this.phoneNumber = number;
    }
}
