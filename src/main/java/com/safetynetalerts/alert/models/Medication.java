package com.safetynetalerts.alert.models;

public class Medication {

    private Long id;
    private MedicalRecord medicalRecord;
    private String name;

    public Medication(String name, MedicalRecord medicalRecordModel){
        this.setName(name);
        this.medicalRecord = medicalRecordModel;
    }

    public Medication(){};

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public MedicalRecord getMedicalRecordModel() {
        return this.medicalRecord;
    }
    public void setMedicalRecordModel(MedicalRecord medicalRecordModel) {
        this.medicalRecord = medicalRecordModel;
    }
}
