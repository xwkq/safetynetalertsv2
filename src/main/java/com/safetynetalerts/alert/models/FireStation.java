package com.safetynetalerts.alert.models;


public class FireStation {

    public void setId(Long id) {
        this.id = id;
    }

    private Long id;
    private String address;
    private String station;

    public FireStation() {}
    public FireStation(String address, String station) {
        this.address = address;
        this.station = station;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }
}
