package com.safetynetalerts.alert.models;

import java.util.List;

public class InputFile {
    public List<Person> persons;
    public List<MedicalRecordsJson> medicalrecords;
    public List<FireStation> firestations;
}
