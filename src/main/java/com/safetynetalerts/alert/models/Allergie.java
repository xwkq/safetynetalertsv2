package com.safetynetalerts.alert.models;


public class Allergie {

    private Long id;
    private MedicalRecord medicalRecord;
    private String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Allergie(String name, MedicalRecord medicalRecordModel){
        this.setName(name);
        this.medicalRecord = medicalRecordModel;
    }

    public Allergie(){};

    public void setId(Long id) {
        this.id = id;
    }

    public MedicalRecord getMedicalRecord() {
        return this.medicalRecord;
    }

    public void setMedicalRecord(MedicalRecord medicalRecordModel) {
        this.medicalRecord = medicalRecordModel;
    }
}
