package com.safetynetalerts.alert.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.safetynetalerts.alert.models.FireStation;
import com.safetynetalerts.alert.models.Person;
import com.safetynetalerts.alert.services.FireStationService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/firestation")
public class FireStationController {
    private final FireStationService fireStationService;

    public FireStationController(FireStationService fireStationService){
        this.fireStationService = fireStationService;
    }


    @PostMapping
    @ResponseBody
    public List<FireStation> create (@RequestBody FireStation fireStationModel){
        return fireStationService.create(fireStationModel);
    }

    @PutMapping(value="/{address}")
    @ResponseBody
    public List<FireStation> update (@RequestBody FireStation fireStationModel, @PathVariable String address) throws JsonProcessingException {
        return fireStationService.update(fireStationModel, address);
    }

    @DeleteMapping(value="/{address}/{station}")
    @ResponseBody
    public List<FireStation> remove(@RequestBody FireStation fireStationModel, @PathVariable String address, @PathVariable String station){
        return this.fireStationService.remove(fireStationModel, address, station);
    }
    @GetMapping(value="/station")
    @ResponseBody
    public List<Person> getPersonByStation(@RequestParam(name = "station") String station){
        return this.fireStationService.getPersonByStation(station);
    }

}
