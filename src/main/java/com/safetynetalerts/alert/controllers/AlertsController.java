package com.safetynetalerts.alert.controllers;

import com.safetynetalerts.alert.models.Contact;
import com.safetynetalerts.alert.models.Person;
import com.safetynetalerts.alert.models.PersonInfo;
import com.safetynetalerts.alert.services.FireStationService;
import com.safetynetalerts.alert.services.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Alerts")
public class AlertsController {
    private final PersonService personService;
    private final FireStationService fireStationService;


    public AlertsController(PersonService personService, FireStationService fireStationService){
        this.personService = personService;
        this.fireStationService = fireStationService;
    }
    @GetMapping(value = "/childAlert")
    @ResponseBody
    public Iterable<Person> getChild(@RequestParam(name = "address") String address) {
        return this.personService.findChild(address);
    }

    @GetMapping(value="/personInfo")
    @ResponseBody
    public List<Person> getPersonInfo(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName) {
        return this.personService.getPersonInfo(firstName, lastName);
    }

    @GetMapping("/communityEmail")
    @ResponseBody
    public List<Contact> getEmails(@RequestParam(name = "city") String city) {
        return this.personService.findEmailByCity(city);
    }

    @GetMapping("/phoneAlert")
    @ResponseBody
    public List<Contact> getPhoneNumber(@RequestParam(name = "station") String station){
        return this.personService.findPhoneByStation(station);
    }

    @GetMapping("/fire")
    @ResponseBody
    public List<Person> getPersonByFireAddress(@RequestParam(name = "address") String address){
        return this.personService.findPersonByFireStationAddress(address);
    }

    @GetMapping("/flood")
    @ResponseBody
    public List<Person> getPersonByStations(@RequestParam(name = "stations") List<String> stations){
        return this.fireStationService.getPersonByStations(stations);
    }
}
