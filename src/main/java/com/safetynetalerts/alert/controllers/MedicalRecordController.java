package com.safetynetalerts.alert.controllers;

import com.safetynetalerts.alert.models.MedicalRecord;
import com.safetynetalerts.alert.services.MedicalRecordService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicalrecords")
public class MedicalRecordController {
    private final MedicalRecordService medicalRecordService;

    public MedicalRecordController(MedicalRecordService medicalRecordService){
        this.medicalRecordService = medicalRecordService;
    }

    @PostMapping
    @ResponseBody
    public List<MedicalRecord> create (@RequestBody MedicalRecord medicalRecordModel){
        return this.medicalRecordService.create(medicalRecordModel);
    }

    @DeleteMapping(value="/{firstName}/{lastName}")
    @ResponseBody
    public List<MedicalRecord> remove (@PathVariable String firstName, @PathVariable String lastName, @RequestBody MedicalRecord medicalRecordModel){
        return this.medicalRecordService.remove(medicalRecordModel, firstName, lastName);
    }

    @PutMapping(value="/{firstName}/{lastName}")
    @ResponseBody
    public List<MedicalRecord>  update (@PathVariable String firstName, @PathVariable String lastName, @RequestBody MedicalRecord medicalRecordModel){
        return this.medicalRecordService.update(medicalRecordModel, firstName, lastName);
    }
}
