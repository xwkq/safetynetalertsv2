package com.safetynetalerts.alert.controllers;

import com.safetynetalerts.alert.models.Person;
import com.safetynetalerts.alert.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/persons")
public class PersonController {
    @Autowired
    private final PersonService personService;

    public PersonController(PersonService personService){
        this.personService = personService;
    }


    @PostMapping
    @ResponseBody
    public List<Person> create (@RequestBody Person personModel){
        return this.personService.create(personModel);
    }

    @DeleteMapping(value="/{firstName}/{lastName}")
    @ResponseBody
    public List<Person> remove (@PathVariable String firstName, @PathVariable String lastName, @RequestBody Person personModel){
        return this.personService.remove(personModel, firstName, lastName);
    }

    @PutMapping(value="/{firstName}/{lastName}")
    @ResponseBody
    public List<Person> update (@PathVariable String firstName, @PathVariable String lastName, @RequestBody Person personModel) {
        return this.personService.update(personModel, firstName, lastName);
    }

}
