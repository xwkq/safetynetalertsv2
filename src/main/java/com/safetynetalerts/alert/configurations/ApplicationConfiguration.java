package com.safetynetalerts.alert.configurations;

import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.safetynetalerts.alert.models.InputFile;
import com.safetynetalerts.alert.utils.DateDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan
@EnableWebMvc
public class ApplicationConfiguration {
    @Value("${jsonPath}")
    public String path;

    @Bean
    public InputFile inputFile(Gson gson) {
        try {
            Reader reader = Files.newBufferedReader(Paths.get(path));
            InputFile inputFileModel = gson.fromJson(reader, InputFile.class);
            reader.close();
            return inputFileModel;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Bean
    public Gson gson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
        return  gsonBuilder.create();
    }
}
