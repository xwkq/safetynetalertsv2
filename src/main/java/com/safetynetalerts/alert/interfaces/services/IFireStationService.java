package com.safetynetalerts.alert.interfaces.services;

import com.safetynetalerts.alert.models.FireStation;
import com.safetynetalerts.alert.models.Person;

import java.util.List;

public interface IFireStationService {
    List<FireStation> findAll();
    List<FireStation> update(FireStation fireStationModel, String address);
    List<FireStation> create(FireStation fireStationModel);
    List<FireStation> remove(FireStation fireStationModel, String address, String station);
    FireStation findByStation(String station);
    List<Person> getPersonByStation(String station);
    List<Person> getPersonByStations(List<String> stations);
}
