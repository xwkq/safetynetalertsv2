package com.safetynetalerts.alert.interfaces.services;

import com.safetynetalerts.alert.models.Person;
import com.safetynetalerts.alert.models.PersonInfo;

import java.util.List;

public interface IPersonService {

    List<Person> getPersonInfo(String firstName, String lastName);
    List<Person> create(Person person);
    List<Person> remove(Person person, String firstName, String lastName);
    List<Person> update(Person personModel, String firstName, String lastName);
    List<Person> persons ();
}
