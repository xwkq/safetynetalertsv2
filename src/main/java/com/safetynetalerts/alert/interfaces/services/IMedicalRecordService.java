package com.safetynetalerts.alert.interfaces.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.safetynetalerts.alert.models.MedicalRecord;

import java.util.List;

public interface IMedicalRecordService {
    List<MedicalRecord> findAll();
    List<MedicalRecord> create(MedicalRecord medicalRecordModel);
    List<MedicalRecord> remove(MedicalRecord medicalRecord, String firstName, String lastName);
    List<MedicalRecord> update(MedicalRecord medicalRecord, String firstName, String lastName);
}
