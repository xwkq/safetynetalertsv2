package com.safetynetalerts.alert.interfaces.repositories;

import com.safetynetalerts.alert.models.MedicalRecordsJson;
import java.util.List;

public interface IMedicalRecordRepository {
    List<MedicalRecordsJson> getMedicalRecords();
}
