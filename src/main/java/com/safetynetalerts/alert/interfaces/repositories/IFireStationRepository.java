package com.safetynetalerts.alert.interfaces.repositories;

import com.safetynetalerts.alert.models.FireStation;
import org.springframework.stereotype.Repository;
import java.util.List;

public interface IFireStationRepository {
    List<FireStation> getFireStations();

}
