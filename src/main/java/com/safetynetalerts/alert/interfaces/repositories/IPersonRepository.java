package com.safetynetalerts.alert.interfaces.repositories;

import com.safetynetalerts.alert.models.Person;

import java.util.List;

public interface IPersonRepository {
    List<Person> getPersons();
}
