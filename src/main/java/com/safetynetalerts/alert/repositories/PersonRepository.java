package com.safetynetalerts.alert.repositories;

import com.safetynetalerts.alert.interfaces.repositories.IPersonRepository;
import com.safetynetalerts.alert.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonRepository implements IPersonRepository {
    @Autowired
    private final InputFile inputFileModel;

    public PersonRepository(InputFile inputFileModel){
        this.inputFileModel = inputFileModel;
    }

    public List<Person> getPersons(){

        return new ArrayList<>(this.inputFileModel.persons);
    }


}
