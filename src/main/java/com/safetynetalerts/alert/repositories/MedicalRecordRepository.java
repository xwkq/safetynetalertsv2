package com.safetynetalerts.alert.repositories;

import com.safetynetalerts.alert.interfaces.repositories.IMedicalRecordRepository;
import com.safetynetalerts.alert.models.InputFile;
import com.safetynetalerts.alert.models.MedicalRecordsJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MedicalRecordRepository implements IMedicalRecordRepository {
    @Autowired
    private final InputFile inputFileModel;
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");


    public MedicalRecordRepository(InputFile inputFileModel) {
        this.inputFileModel = inputFileModel;
    }

    public List<MedicalRecordsJson> getMedicalRecords(){
        return new ArrayList<>(this.inputFileModel.medicalrecords);
    }
}
