package com.safetynetalerts.alert.repositories;

import com.safetynetalerts.alert.interfaces.repositories.IFireStationRepository;
import com.safetynetalerts.alert.models.FireStation;
import com.safetynetalerts.alert.models.InputFile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FireStationRepository implements IFireStationRepository {
    private final InputFile inputFileModel;

    public FireStationRepository(InputFile inputFileModel) {
        this.inputFileModel = inputFileModel;
    }

    @Override
    public List<FireStation> getFireStations(){
        return new ArrayList<>(this.inputFileModel.firestations);
    }
}
