package com.safetynetalerts.alert.batch;

import com.safetynetalerts.alert.interfaces.repositories.IFireStationRepository;
import com.safetynetalerts.alert.interfaces.repositories.IMedicalRecordRepository;
import com.safetynetalerts.alert.interfaces.repositories.IPersonRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class JsonTreatment implements ApplicationRunner {

    private final IPersonRepository personRepository;
    private final IMedicalRecordRepository medicalRecordRepository;
    private final IFireStationRepository fireStationRepository;

    public JsonTreatment(IPersonRepository personRepository, IMedicalRecordRepository medicalRecordRepository, IFireStationRepository fireStationRepository){

        this.personRepository = personRepository;
        this.medicalRecordRepository = medicalRecordRepository;
        this.fireStationRepository = fireStationRepository;
    }

    public void run(ApplicationArguments args) throws Exception {
        personRepository.getPersons();
        medicalRecordRepository.getMedicalRecords();
        fireStationRepository.getFireStations();
    }
}
